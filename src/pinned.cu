#include "../include/pinned.cuh"
#include "cuda.h"
#include "cuda_fp16.h"
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"
#include "kernels.cu"

void writeTimeMeasurement_pinned(std::vector<cudaEvent_t> events,
                                 std::ofstream &outputFile, long int size);

void kernel_add_comp_pinned(int samplesize, char *datatype, char *outdir) {

  long int N[] = {10000000,  50000000,  100000000, 125000000,
                  250000000, 500000000, 1000000000};
  std::string outdirpath = "/home/koehler/sparse-matrix-gpu/cuda-test-cleaned/";
  outdirpath += outdir;
  std::string outfilename = outdirpath;
  outfilename += std::string("/");
  int threads = 1024;

  std::vector<std::string> eventNames(6);
  eventNames[0] = "buffer_a";
  eventNames[1] = "buffer_b";
  eventNames[2] = "buffer_c_alloc";
  eventNames[3] = "kernel";
  eventNames[4] = "buffer_c_to_host";
  eventNames[5] = "wallclock_time";

  for (int n = 0; n < 7; ++n) {
    std::string size_string = std::to_string(N[n]);
    std::ofstream outputFile(outfilename + datatype + "_" + size_string);
    outputFile << "#" << eventNames[0] << " \t" << eventNames[1] << " \t"
               << eventNames[2] << " \t" << eventNames[3] << " \t"
               << eventNames[4] << " \t" << eventNames[5] << std::endl;
    if (strcmp(datatype, "double") == 0) {

      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        double *host_a_d, *host_b_d, *host_c_d;
        cudaMallocHost((void **)&host_a_d, sizeof(double) * N[n]);
        cudaMallocHost((void **)&host_b_d, sizeof(double) * N[n]);
        cudaMallocHost((void **)&host_c_d, sizeof(double) * N[n]);
        double *d_a, *d_b, *d_c;
        for (int i = 0; i < N[n]; ++i) {
          host_a_d[i] = 1;
          host_b_d[i] = 1;
        }
        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);
        cudaDeviceSynchronize();

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] * sizeof(double));
        cudaMemcpy(d_a, host_a_d, N[n] * sizeof(double),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] * sizeof(double));
        cudaMemcpy(d_b, host_b_d, N[n] * sizeof(double),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] * sizeof(double));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_comp_kernel<<<ceil((N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                             N[n]);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_d, d_c, N[n] * sizeof(double),
                   cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        for (auto e : events)
          cudaEventDestroy(e);
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_d);
        cudaFreeHost(host_b_d);
        cudaFreeHost(host_c_d);
        cudaDeviceSynchronize();
      }
    } else if (strcmp(datatype, "float") == 0) {

      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        float *host_a_f, *host_b_f, *host_c_f;
        cudaMallocHost((void **)&host_a_f, sizeof(float) * N[n]);
        cudaMallocHost((void **)&host_b_f, sizeof(float) * N[n]);
        cudaMallocHost((void **)&host_c_f, sizeof(float) * N[n]);
        float *d_a, *d_b, *d_c;
        for (int i = 0; i < N[n]; ++i) {
          host_a_f[i] = 1;
          host_b_f[i] = 1;
        }
        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);
        cudaDeviceSynchronize();

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] * sizeof(float));
        cudaMemcpy(d_a, host_a_f, N[n] * sizeof(float), cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] * sizeof(float));
        cudaMemcpy(d_b, host_b_f, N[n] * sizeof(float), cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] * sizeof(float));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_comp_kernel<<<ceil((N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                             N[n]);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_f, d_c, N[n] * sizeof(float), cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        for (auto e : events)
          cudaEventDestroy(e);
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_f);
        cudaFreeHost(host_b_f);
        cudaFreeHost(host_c_f);
        cudaDeviceSynchronize();
      }
    } else if (strcmp(datatype, "half") == 0) {
      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        half *host_a_f, *host_b_f, *host_c_f;
        cudaMallocHost((void **)&host_a_f, sizeof(half) * N[n]);
        cudaMallocHost((void **)&host_b_f, sizeof(half) * N[n]);
        cudaMallocHost((void **)&host_c_f, sizeof(half) * N[n]);
        half *d_a, *d_b, *d_c;
        for (int i = 0; i < N[n]; ++i) {
          host_a_f[i] = __float2half(1.0);
          host_b_f[i] = __float2half(1.0);
        }
        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);
        cudaDeviceSynchronize();

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] * sizeof(half));
        cudaMemcpy(d_a, host_a_f, N[n] * sizeof(half), cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] * sizeof(half));
        cudaMemcpy(d_b, host_b_f, N[n] * sizeof(half), cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] * sizeof(half));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_comp_kernel<<<ceil((N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                             N[n]);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_f, d_c, N[n] * sizeof(half), cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        for (auto e : events)
          cudaEventDestroy(e);
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_f);
        cudaFreeHost(host_b_f);
        cudaFreeHost(host_c_f);
        cudaDeviceSynchronize();
      }
    } else if (strcmp(datatype, "half2") == 0) {
      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        half2 *host_a_f, *host_b_f, *host_c_f;
        cudaMallocHost((void **)&host_a_f, sizeof(half2) * N[n] / 2);
        cudaMallocHost((void **)&host_b_f, sizeof(half2) * N[n] / 2);
        cudaMallocHost((void **)&host_c_f, sizeof(half2) * N[n] / 2);
        half2 *d_a, *d_b, *d_c;
        float2 init = make_float2(1, 1);
        for (int i = 0; i < N[n] / 2; ++i) {
          host_a_f[i] = __float22half2_rn(init);
          host_b_f[i] = __float22half2_rn(init);
        }

        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] / 2 * sizeof(half2));
        cudaMemcpy(d_a, host_a_f, N[n] / 2 * sizeof(half2),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] / 2 * sizeof(half2));
        cudaMemcpy(d_b, host_b_f, N[n] / 2 * sizeof(half2),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] / 2 * sizeof(half2));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_comp_kernel<<<ceil((N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                             N[n] / 2);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_f, d_c, N[n] / 2 * sizeof(half2),
                   cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_f);
        cudaFreeHost(host_b_f);
        cudaFreeHost(host_c_f);
        for (auto e : events)
          cudaEventDestroy(e);
      }
    } else {
      std::cout << "###no valid datatype###" << std::endl;
    }
  }
}

void kernel_add_pinned(int samplesize, char *datatype, char *outdir) {
  long int N[] = {10000000,  50000000,  100000000, 125000000,
                  250000000, 500000000, 1000000000};
  std::string outdirpath = "/home/koehler/sparse-matrix-gpu/cuda-test-cleaned/";
  outdirpath += outdir;
  std::string outfilename = outdirpath;
  outfilename += std::string("/");
  int threads = 1024;

  std::vector<std::string> eventNames(6);
  eventNames[0] = "buffer_a";
  eventNames[1] = "buffer_b";
  eventNames[2] = "buffer_c_alloc";
  eventNames[3] = "kernel";
  eventNames[4] = "buffer_c_to_host";
  eventNames[5] = "wallclock_time";

  for (int n = 0; n < 7; ++n) {
    std::string size_string = std::to_string(N[n]);
    std::ofstream outputFile(outfilename + datatype + "_" + size_string);
    outputFile << "#" << eventNames[0] << " \t" << eventNames[1] << " \t"
               << eventNames[2] << " \t" << eventNames[3] << " \t"
               << eventNames[4] << " \t" << eventNames[5] << std::endl;
    if (strcmp(datatype, "double") == 0) {

      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        double *host_a_d, *host_b_d, *host_c_d;
        cudaMallocHost((void **)&host_a_d, sizeof(double) * N[n]);
        cudaMallocHost((void **)&host_b_d, sizeof(double) * N[n]);
        cudaMallocHost((void **)&host_c_d, sizeof(double) * N[n]);
        double *d_a, *d_b, *d_c;
        for (int i = 0; i < N[n]; ++i) {
          host_a_d[i] = 1;
          host_b_d[i] = 1;
        }
        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);
        cudaDeviceSynchronize();

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] * sizeof(double));
        cudaMemcpy(d_a, host_a_d, N[n] * sizeof(double),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] * sizeof(double));
        cudaMemcpy(d_b, host_b_d, N[n] * sizeof(double),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] * sizeof(double));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_kernel<<<(int)ceil(float(N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                                  N[n]);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_d, d_c, N[n] * sizeof(double),
                   cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        for (auto e : events)
          cudaEventDestroy(e);
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_d);
        cudaFreeHost(host_b_d);
        cudaFreeHost(host_c_d);
        cudaDeviceSynchronize();
      }
    } else if (strcmp(datatype, "float") == 0) {

      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        float *host_a_f, *host_b_f, *host_c_f;
        cudaMallocHost((void **)&host_a_f, sizeof(float) * N[n]);
        cudaMallocHost((void **)&host_b_f, sizeof(float) * N[n]);
        cudaMallocHost((void **)&host_c_f, sizeof(float) * N[n]);
        float *d_a, *d_b, *d_c;
        for (int i = 0; i < N[n]; ++i) {
          host_a_f[i] = 1;
          host_b_f[i] = 1;
        }
        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);
        cudaDeviceSynchronize();

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] * sizeof(float));
        cudaMemcpy(d_a, host_a_f, N[n] * sizeof(float), cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] * sizeof(float));
        cudaMemcpy(d_b, host_b_f, N[n] * sizeof(float), cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] * sizeof(float));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_kernel<<<(int)ceil(float(N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                                  N[n]);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_f, d_c, N[n] * sizeof(float), cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_f);
        cudaFreeHost(host_b_f);
        cudaFreeHost(host_c_f);
        for (auto e : events)
          cudaEventDestroy(e);
        cudaDeviceSynchronize();
      }

    } else if (strcmp(datatype, "half") == 0) {
      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        half *host_a_f, *host_b_f, *host_c_f;
        cudaMallocHost((void **)&host_a_f, sizeof(half) * N[n]);
        cudaMallocHost((void **)&host_b_f, sizeof(half) * N[n]);
        cudaMallocHost((void **)&host_c_f, sizeof(half) * N[n]);
        half *d_a, *d_b, *d_c;
        for (int i = 0; i < N[n]; ++i) {
          host_a_f[i] = __float2half(1.0);
          host_b_f[i] = __float2half(1.0);
        }
        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);
        cudaDeviceSynchronize();

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] * sizeof(half));
        cudaMemcpy(d_a, host_a_f, N[n] * sizeof(half), cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] * sizeof(half));
        cudaMemcpy(d_b, host_b_f, N[n] * sizeof(half), cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] * sizeof(half));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_kernel<<<ceil((N[n]) / threads), threads>>>(d_a, d_b, d_c, N[n]);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_f, d_c, N[n] * sizeof(half), cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        for (auto e : events)
          cudaEventDestroy(e);
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_f);
        cudaFreeHost(host_b_f);
        cudaFreeHost(host_c_f);
        cudaDeviceSynchronize();
      }
    } else if (strcmp(datatype, "half2") == 0) {
      for (int run = 0; run < samplesize; ++run) {
        std::clock_t c_start = std::clock();
        half2 *host_a_f, *host_b_f, *host_c_f;
        cudaMallocHost((void **)&host_a_f, sizeof(half2) * N[n] / 2);
        cudaMallocHost((void **)&host_b_f, sizeof(half2) * N[n] / 2);
        cudaMallocHost((void **)&host_c_f, sizeof(half2) * N[n] / 2);
        half2 *d_a, *d_b, *d_c;
        float2 init = make_float2(1, 1);
        for (int i = 0; i < N[n] / 2; ++i) {
          host_a_f[i] = __float22half2_rn(init);
          host_b_f[i] = __float22half2_rn(init);
        }

        std::vector<cudaEvent_t> events(10);
        for (int i = 0; i < events.size(); ++i)
          cudaEventCreate(&events[i]);

        cudaEventRecord(events[0]);
        cudaMalloc(&d_a, N[n] / 2 * sizeof(half2));
        cudaMemcpy(d_a, host_a_f, N[n] / 2 * sizeof(half2),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[5]);
        cudaEventSynchronize(events[5]);

        cudaEventRecord(events[1]);
        cudaMalloc(&d_b, N[n] / 2 * sizeof(half2));
        cudaMemcpy(d_b, host_b_f, N[n] / 2 * sizeof(half2),
                   cudaMemcpyHostToDevice);
        cudaEventRecord(events[6]);
        cudaEventSynchronize(events[6]);

        cudaEventRecord(events[2]);
        cudaMalloc(&d_c, N[n] / 2 * sizeof(half2));
        cudaEventRecord(events[7]);
        cudaEventSynchronize(events[7]);

        cudaEventRecord(events[3]);
        // Launch add() kernel on GPU
        add_kernel<<<ceil((N[n]) / threads), threads>>>(d_a, d_b, d_c,
                                                        N[n] / 2);
        cudaEventRecord(events[8]);
        cudaEventSynchronize(events[8]);

        cudaEventRecord(events[4]);
        // Copy result back to the host
        cudaMemcpy(host_c_f, d_c, N[n] / 2 * sizeof(half2),
                   cudaMemcpyDeviceToHost);
        cudaEventRecord(events[9]);
        cudaEventSynchronize(events[9]);
        cudaDeviceSynchronize();

        std::clock_t c_end = std::clock();
        writeTimeMeasurement_pinned(events, outputFile, N[n]);
        outputFile << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << std::endl;
        // Cleanup
        cudaFree(d_a);
        cudaFree(d_b);
        cudaFree(d_c);
        cudaFreeHost(host_a_f);
        cudaFreeHost(host_b_f);
        cudaFreeHost(host_c_f);
        for (auto e : events)
          cudaEventDestroy(e);
      }
    } else {
      std::cout << "###no valid datatype###" << std::endl;
    }
  }
}

void writeTimeMeasurement_pinned(std::vector<cudaEvent_t> events,
                                 std::ofstream &outputFile, long int size) {

  for (int i = 0; i < events.size() / 2; ++i) {
    float millisecs = 0.0;
    cudaEventElapsedTime(&millisecs, events[i], events[i + 5]);
    outputFile << millisecs << " \t";
  }
}
