#include "cuda.h"
#include "cuda_fp16.h"
#include "cuda_runtime.h"
#include "cuda_runtime_api.h"

template <typename T> __device__ __forceinline__ T computation_div(T a, T b) {
  return ((a * b + a * b + a * b + a * b) / (a * b + a * b + a * b + a * b));
}
template <typename T> __device__ __forceinline__ T computation_minus(T a, T b) {
  return ((a * b + a * b + a * b + a * b) - (a * b + a * b + a * b + a * b));
}
template <> __device__ __forceinline__ half computation_minus(half a, half b) {
  return __hadd(
      -__hadd(__hadd(__hadd(__hmul(a, b), __hmul(a, b)), __hmul(a, b)),
              __hmul(a, b)),
      __hadd(__hadd(__hadd(__hmul(a, b), __hmul(a, b)), __hmul(a, b)),
             __hmul(a, b)));
}
template <>
__device__ __forceinline__ half2 computation_minus(half2 a, half2 b) {
  return __hadd2(-__hadd2(__hmul2(a, b), __hmul2(a, b)),
                 __hadd2(__hmul2(a, b), __hmul2(a, b)));
  // return 1;
}
template <> __device__ __forceinline__ half computation_div(half a, half b) {
  return __hdiv(__hadd(__hadd(__hadd(__hmul(a, b), __hmul(a, b)), __hmul(a, b)),
                       __hmul(a, b)),
                __hadd(__hadd(__hadd(__hmul(a, b), __hmul(a, b)), __hmul(a, b)),
                       __hmul(a, b)));
}

template <> __device__ __forceinline__ half2 computation_div(half2 a, half2 b) {
  return __h2div(__hadd2(__hmul2(a, b), __hmul2(a, b)),
                 __hadd2(__hmul2(a, b), __hmul2(a, b)));
}
template <typename T>
__global__ void add_comp_kernel(T *a, T *b, T *c, long int N) {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < N) {
    T a_d = a[idx];
    T b_d = b[idx];
    T c_d = c[idx];
    for (int i = 0; i < 5000; ++i) {
      c_d += computation_minus(a_d, b_d);
    }
    c[idx] = c_d;
  }
}
template <typename T> __global__ void add_kernel(T *a, T *b, T *c, long int N) {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  if (idx < N) {
    c[idx] = a[idx] + b[idx];
  }
}
// template<>
//__global__ void add_kernel(half* a, half* b, half* c, long int N){
//	int idx = blockIdx.x*blockDim.x+threadIdx.x;
//	if(idx < N){
//		c[idx] = __hadd(a[idx], b[idx]);
//	}
//}
// template<>
//__global__ void add_kernel(half2* a, half2* b, half2* c, long int N){
//	int idx = blockIdx.x*blockDim.x+threadIdx.x;
//	if(idx < N){
//		c[idx] = __hadd2(a[idx], b[idx]);
//	}
//}
