# CUDA Beginner's Guide

## Project Structure

Project structure is crucial for any larger coding project. 
Hence, I propose the following setup, which is taken over from [this GitHub Repo](https://github.com/TravisWThompson1/Makefile_Example_CUDA_CPP_To_Executable).
The Makefile will compile .cpp and their associated .h files with gcc, while all .cu and .cuh files are compiled with nvcc. So, if you want to use nvcc solely, name all your files .cu and .cuh, respectively.

Readme from the repo:

```
|--> Project/
       |--> Makefile
       |--> src/ (source files)
       |--> include/ (header files)
       |--> bin/
```

For example, you could have multiple .cpp and .cu files in the source directory (src/), multiple .h and .cuh header files in the header directory (include/), and the file with the main() function (titled main.cpp in this example) in the base directory. An example of this could be the following:

```
|--> Project/
       |--> Makefile
       |--> main.cpp
       |--> src/
              |--> file1.cpp
              |--> file2.cpp
              |--> file_cuda1.cu
              |--> file_cuda2.cu
       |--> include/
              |--> file1.h
              |--> file2.h
              |--> file1.cuh
              |--> file2.cuh
       |--> bin/
```

The Makefile in this repo compiles a .cu and .cuh file called cuda_kernel.cu and cuda_kernel.cuh, which contains a simple CUDA device function. The cuda_kernel.cuh header file is included in the main.cpp file and called in the main() function. The Makefile will compile all the CUDA and C++ source files in src/ first, then compile the main.cpp file, and finally link them together.

To use this Makefile in your own projects, a few parameters may need to be changed. First, the CUDA root directory should be specified for your machine.

```
###########################################################

## USER SPECIFIC DIRECTORIES ##

# CUDA directory:
CUDA_ROOT_DIR=/usr/local/cuda

##########################################################
```

In addition, any changes to compiler options, flags, or alias should be made.

```
##########################################################

## CC COMPILER OPTIONS ##

# CC compiler options:
CC=g++
CC_FLAGS=
CC_LIBS=

##########################################################

## NVCC COMPILER OPTIONS ##

# NVCC compiler options:
NVCC=nvcc
NVCC_FLAGS=
NVCC_LIBS=

##########################################################
```

The project's file structure can be changed if necessary.

```
##########################################################

## Project file structure ##

# Source file directory:
SRC_DIR = src

# Object file directory:
OBJ_DIR = bin

# Include header file diretory:
INC_DIR = include

##########################################################
```

Finally, and most important, all of the object files that will be created should be listed in the OBJS variable with a space between each entry. Each source file in your project should be listed here as $(OBJ_DIR)/yourfile.o , these are all of the compiled source files (object files) that will be linked together to create the executable file.

```
##########################################################

## Make variables ##

# Target executable name:
EXE = run_test

# Object files:
OBJS = $(OBJ_DIR)/main.o $(OBJ_DIR)/cuda_kernel.o

##########################################################
```
## Building the Program
You need a working version of ``gcc`` and ``nvcc`` to compile the program. Before compiling, you should change the ``outdirpath`` variables in ``src/pinned.cu`` and ``src/nonpinned.cu`` in line 13, 261 and 13, 247, respectively, to the path of this repo. 

Type ``make`` to compile the program.

## Usage of the compiled program

The compiled program ``run_test`` takes five input arguments.

 1. type of memory, i.e. ``pinned`` or ``non-pinned``
 2. choose between a simple vector addition (``add``) or a computationally more expensive kernel (``comp``)
 3. sample size, how often the routine should be executed for each vector size
 4. datatype to be used, ``double`` ``float`` ``half`` or ``half2``
 5. output directory, where the executable is your root directory

So to conclude, the following would be a run with non-pinned memory, the computationally more intense kernel, routine is executed 10 times for each vector size for the datatype half and the output directory is ``measurements/test``

```bash
./run_test non-pinned comp 10 half measurements/test
```

## What the Program does

In general, the program allocates three vectors, whereby two of them are initially allocated on the host and transferred to the device. The third vector is directly allocated on the device. After some computation the program sends the three vectors back from the device to the host. For this, all times are measured via ``cudaEvents``.

Now, different types of memory usage, computations and datatypes are possible.

### Memory Usage

In CUDA there exist several types of memory allocation. There is an "automated" one with ``cudaMallocManaged`` which uses a unified memory space, i.e. device and host share the same memory address space

```cuda
  float* x,y;
  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));
```

Array ``x`` and ``y`` can be accessed from the host **and** the device. 
More information about managed memory can be found here: [CUDA Blogpost about Unified Memory](https://devblogs.nvidia.com/unified-memory-cuda-beginners/). Unified memory is easy to use, but will likely kill your performance. At most, this should be used for prototyping.


A completely other approach is explicit allocation in CUDA, which works with pinned or non-pinned memory. The latter one, *****non-pinned memory*****, is the default method, which can be seen in various CUDA tutorials.

```cuda
  float *x, *y, *d_x, *d_y;
  x = (float*)malloc(N*sizeof(float));
  y = (float*)malloc(N*sizeof(float));

  cudaMalloc(&d_x, N*sizeof(float)); 
  cudaMalloc(&d_y, N*sizeof(float));
```

The code example show how four arrays are allocated ``x,y,d_x`` and ``d_y``. You could do now some initialization computation with ``x`` and ``y`` and send the entries afterwards to ``d_x`` and ``d_y``.

```cuda
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
```

Never forget to clean your memory after your computations.

```cuda
  cudaFree(d_x);
  cudaFree(d_y);
  free(x);
  free(y);
```

*****pinned memory***** works slightly different, especially from the hardware perspective. A detailed explanation about the difference can be found here: [CUDA Blogpost about Efficient Datatransfer](https://devblogs.nvidia.com/how-optimize-data-transfers-cuda-cc/). The bottom line is that you will notice effects of paging, when using non-pinned memory.

```cuda
  float *x, *y, d_x, d_y;
  cudaMallocHost((void**)&x,sizeof(float)*N);
  cudaMallocHost((void**)&y,sizeof(float)*N);
  //initialize x and y on the host as you need
  cudaMalloc(&d_x, N[n]*sizeof(float));
  cudaMalloc(&d_y, N[n]*sizeof(float));
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
```

The above code shows a full example. The difference is that you already employ CUDA calls when allocating the memory on the host via ``cudaMallocHost``, by this you omit the need of paged host memory.

If your datatransfer performance is a negligible part of your overall performance, then choose **non-pinned** memory, since you can allocate more memory and thus have more freedom. 
If you need full performance, especially realiable performance when benchmarking, go with the **pinned** option. 

Below the two images compare the datatransfer on a V100 where the vector size is scaled to memory limits.

<img src="notebooks/figures/vector_a_nonpinned.png"/>
<img src="notebooks/figures/vector_a_pinned.png"/>