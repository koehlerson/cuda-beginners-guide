#include <string.h>

#include <iostream>
#include <vector>

#include "include/nonpinned.cuh"
#include "include/pinned.cuh"

int main(int argc, char **argv) {
    if (argc < 2) {
	std::cout << "###ABORT NOT ENOUGH ARGUMENTS###" << std::endl;
	std::cout << "specify the kernel" << std::endl;
    }

    // argv[1] first command line argument pinned or nonpinned memory
    // argv[2] second command line argument kernel add or comp
    // argv[3] third command line argument samplesize
    // argv[4] fourth command line argument format
    // argv[5] fifth command line argument output dir

    else {
	if (strcmp(argv[1], "pinned") == 0) {
	    if (strcmp(argv[2], "add") == 0) {
		kernel_add_pinned(atoi(argv[3]), argv[4], argv[5]);
	    }

	    else if (strcmp(argv[2], "comp") == 0) {
		kernel_add_comp_pinned(atoi(argv[3]), argv[4], argv[5]);
	    } else {
		std::cout << "###ABORT NO KERNEL###" << std::endl;
		std::cout << "no valid kernel chosen" << std::endl;
		std::cout << "#" << argv[1] << "#" << std::endl;
		std::cout << "#" << argv[2] << "#" << std::endl;
		std::cout << "#" << argv[3] << "#" << std::endl;
	    }
	} else if (strcmp(argv[1], "non-pinned") == 0) {
	    if (strcmp(argv[2], "add") == 0) {
		kernel_add_nonpinned(atoi(argv[3]), argv[4], argv[5]);
	    }

	    else if (strcmp(argv[2], "comp") == 0) {
		kernel_add_comp_nonpinned(atoi(argv[3]), argv[4], argv[5]);
	    } else {
		std::cout << "###ABORT NO KERNEL###" << std::endl;
		std::cout << "no valid kernel chosen" << std::endl;
		std::cout << "#" << argv[1] << "#" << std::endl;
		std::cout << "#" << argv[2] << "#" << std::endl;
		std::cout << "#" << argv[3] << "#" << std::endl;
	    }

	} else {
	    std::cout << "###ABORT NO VALID MEMORY OPTION###" << std::endl;
	    std::cout << "choose between pinned and non-pinned" << std::endl;
	}
    }
    return 0;
}
