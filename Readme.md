# CUDA Beginner's Guide

## Table of Contents
 
 1. [Project Structure](##project-structure)
 2. [Building the Program](##building-the-program)
 3. [Usage of the compiled Program](##usage-of-the-compiled-program)
 4. [What the Program does](##what-the-program-does)
     1. [Memory usage](###memory-usage)
     2. [Computations](###computations)
 5. [Plot Measurements](##plot-measurements)


## Project Structure

Project structure is crucial for any larger coding project. 
Hence, I propose the following setup, which is taken over from [this GitHub Repo](https://github.com/TravisWThompson1/Makefile_Example_CUDA_CPP_To_Executable).
The Makefile will compile .cpp and their associated .h files with gcc, while all .cu and .cuh files are compiled with nvcc. So, if you want to use nvcc solely, name all your files .cu and .cuh, respectively.

Readme from the repo:

```
|--> Project/
       |--> Makefile
       |--> src/ (source files)
       |--> include/ (header files)
       |--> bin/
```

For example, you could have multiple .cpp and .cu files in the source directory (src/), multiple .h and .cuh header files in the header directory (include/), and the file with the main() function (titled main.cpp in this example) in the base directory. An example of this could be the following:

```
|--> Project/
       |--> Makefile
       |--> main.cpp
       |--> src/
              |--> file1.cpp
              |--> file2.cpp
              |--> file_cuda1.cu
              |--> file_cuda2.cu
       |--> include/
              |--> file1.h
              |--> file2.h
              |--> file1.cuh
              |--> file2.cuh
       |--> bin/
```

The Makefile in this repo compiles a .cu and .cuh file called cuda_kernel.cu and cuda_kernel.cuh, which contains a simple CUDA device function.
The cuda_kernel.cuh header file is included in the main.cpp file and called in the main() function.
The Makefile will compile all the CUDA and C++ source files in src/ first, then compile the main.cpp file, and finally link them together.

To use this Makefile in your own projects, a few parameters may need to be changed. First, the CUDA root directory should be specified for your machine.

```
###########################################################

## USER SPECIFIC DIRECTORIES ##

# CUDA directory:
CUDA_ROOT_DIR=/usr/local/cuda

##########################################################
```

In addition, any changes to compiler options, flags, or alias should be made.

```
##########################################################

## CC COMPILER OPTIONS ##

# CC compiler options:
CC=g++
CC_FLAGS=
CC_LIBS=

##########################################################

## NVCC COMPILER OPTIONS ##

# NVCC compiler options:
NVCC=nvcc
NVCC_FLAGS=
NVCC_LIBS=

##########################################################
```

The project's file structure can be changed if necessary.

```
##########################################################

## Project file structure ##

# Source file directory:
SRC_DIR = src

# Object file directory:
OBJ_DIR = bin

# Include header file diretory:
INC_DIR = include

##########################################################
```

Finally, and most important, all of the object files that will be created should be listed in the OBJS variable with a space between each entry. Each source file in your project should be listed here as $(OBJ_DIR)/yourfile.o , these are all of the compiled source files (object files) that will be linked together to create the executable file.

```
##########################################################

## Make variables ##

# Target executable name:
EXE = run_test

# Object files:
OBJS = $(OBJ_DIR)/main.o $(OBJ_DIR)/cuda_kernel.o

##########################################################
```
## Building the Program
You need a working version of ``gcc`` and ``nvcc`` to compile the program. Before compiling, you should change the ``outdirpath`` variables in ``src/pinned.cu`` and ``src/nonpinned.cu`` in line 13, 261 and 13, 247, respectively, to the path of this repo. 

Before ``make`` you have to create the dir ``bin``, where the binary objects are stored. Otherwise, make will return an error that the directory does not exist. 
After ``mkdir bin`` type ``make`` to compile the program.

## Usage of the compiled program

The compiled program ``run_test`` takes five input arguments.

 1. type of memory, i.e. ``pinned`` or ``non-pinned``
 2. choose between a simple vector addition (``add``) or a computationally more expensive kernel (``comp``)
 3. sample size, how often the routine should be executed for each vector size
 4. datatype to be used, ``double`` ``float`` ``half`` or ``half2``
 5. output directory, where the executable is your root directory

So to conclude, the following would be a run with non-pinned memory, the computationally more intense kernel, routine is executed 10 times for each vector size for the datatype half and the output directory is ``measurements/test``

```bash
./run_test non-pinned comp 10 half measurements/test
```

## What the Program does

In general, the program allocates three vectors, whereby two of them are initially allocated on the host and transferred to the device. The third vector is directly allocated on the device. After some computation the program sends the three vectors back from the device to the host. For this, all times are measured via ``cudaEvents``.

Now, different types of memory usage, computations and datatypes are possible.

### Memory Usage

In CUDA there exist several types of memory allocation. There is an "automated" one with ``cudaMallocManaged`` which uses a unified memory space, i.e. device and host share the same memory address space

```cpp
  float* x,y;
  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));
```

Array ``x`` and ``y`` can be accessed from the host **and** the device. 
More information about managed memory can be found here: [CUDA Blogpost about Unified Memory](https://devblogs.nvidia.com/unified-memory-cuda-beginners/). Unified memory is easy to use, but will likely kill your performance. At most, this should be used for prototyping.


A completely other approach is explicit allocation in CUDA, which works with pinned or non-pinned memory. The latter one, *****non-pinned memory*****, is the default method, which can be seen in various CUDA tutorials.

```cpp
  float *x, *y, *d_x, *d_y;
  x = (float*)malloc(N*sizeof(float));
  y = (float*)malloc(N*sizeof(float));

  cudaMalloc(&d_x, N*sizeof(float)); 
  cudaMalloc(&d_y, N*sizeof(float));
```

The code example show how four arrays are allocated ``x,y,d_x`` and ``d_y``. You could do now some initialization computation with ``x`` and ``y`` and send the entries afterwards to ``d_x`` and ``d_y``.

```cpp
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
```

Never forget to clean your memory after your computations.

```cpp
  cudaFree(d_x);
  cudaFree(d_y);
  free(x);
  free(y);
```

*****pinned memory***** works slightly different, especially from the hardware perspective. A detailed explanation about the difference can be found here: [CUDA Blogpost about Efficient Datatransfer](https://devblogs.nvidia.com/how-optimize-data-transfers-cuda-cc/). The bottom line is that you will notice effects of paging, when using non-pinned memory.

```cpp
  float *x, *y, d_x, d_y;
  cudaMallocHost((void**)&x,sizeof(float)*N);
  cudaMallocHost((void**)&y,sizeof(float)*N);
  //initialize x and y on the host as you need
  cudaMalloc(&d_x, N[n]*sizeof(float));
  cudaMalloc(&d_y, N[n]*sizeof(float));
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
```

The above code shows a full example. The difference is that you already employ CUDA calls when allocating the memory on the host via ``cudaMallocHost``, by this you omit the need of paged host memory.

If your datatransfer performance is a negligible part of your overall performance, then choose **non-pinned** memory, since you can allocate more memory and thus have more freedom. 
If you need full performance, especially realiable performance when benchmarking, go with the **pinned** option. 

Below the two images compare the datatransfer on a V100 where the vector size is scaled to memory limits.


![](notebooks/figures/vector_a_nonpinned.png){width=90%}


![](notebooks/figures/vector_a_pinned.png){width=90%}


The vertical bars visualize the standard deviation of the measurements. You can see that the pinned memory yields a constant bandwidth, while the datatransfer with non-pinned memory fluctuates.


![](notebooks/figures/bandwidth_nonpinned.png){width=90%}


![](notebooks/figures/bandwidth_pinned.png){width=90%}


Besides the fluctuation the overall speed of non-pinned memory is also slower. Above the two plots compare the bandwidth from host to device. Note that the V100 is connected with a PCIe x16 3.0 which has a peak performance of 15.75 GB/s ([see the wiki article on PCIe](https://en.wikipedia.org/wiki/PCI_Express#History_and_revisions)). So, pinned memory is the best choice to obtain a bandwidth that is close to hardware limit.

### Computations

While all functions in ``src/pinned.cu`` and ``src/nonpinned.cu`` are not templated by intention, the ``src/kernels.cu`` sourcefile uses templates. 
The file consists of two different template functions that are specialized for ``half`` and ``half2``, in order to use their arithmetic CUDA intrinsics.

The kernel called ``add_kernel`` adds two vectors and stores the result in a third.
$$c_i = a_i+b_i$$

The kernel named ``add_comp_kernel`` stores the values $a_i, b_i$ and $c_i$ in $a_d, b_d$ and $c_d$ once, computes
$$c_d=\sum_{j=0}^{5000} \frac{a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d}{a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d} \quad \text{or}$$ 
$$\quad c_d=\sum_{j=0}^{5000} (a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d)-(a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d+a_d\cdot b_d)$$

You can switch them by changing the invoked function in ``src/kernels.cu add_comp_kernel`` from ``computation_minus`` to ``computation_div``. 
It is crucial to keep in mind how the hardware works. An outstanding example is for instance the difference of the kernel performance between the ``computation_minus`` (second formula above) and ``computation_div`` (first formula). The implementation of division for halfs is extremely poor and hence kills the whole performance. Furthermore, the V100 has only 32-bit registers, so there is no performance gain when using plain ``halfs``. You need to store two halfs in one 32-bit register to utilize the advantages of halfs, also called vectorization, denoted by ``half2``. 


![](notebooks/figures/kernel_minus.png){width=90%}


![](notebooks/figures/kernel_div.png){width=90%}


The plots show that either the division implementation is bad or the compiler optimisation is not consistent, because the half performance is worse than doubles. Division is in general a complicated operation for GPUs and since halfs are rather new, the drivers and implementations are maybe not fully optimized, yet. 

For the minus computation you can see the effects of the 32-bit registers. While plain half performs in the very same way as float (float = 32bits, so fills up one register), the vectorized half version (half2) yields the expected speed up.

### How its measured

You have several options to implement performance metrics (or query them). You can use wallclock times from the host, gpu times via ``cudaEvents`` or use the nvidia profiler ``nvprof``.

```cpp
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  cudaMemcpy(d_x, x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, y, N*sizeof(float), cudaMemcpyHostToDevice);
  
  cudaEventRecord(start);
  saxpy<<<(N+255)/256, 256>>>(N, 2.0f, d_x, d_y);
  cudaEventRecord(stop);
  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
```

measures the time with ``cudaEvents`` by wrapping the operation to be measured between two events and afterwards invoke ``cudaEventElapsedTime`` to get the time elapsed. The returned elapsed time is in millisecond. This implementation has a resolution of one half microsecond. 


The same wrapping can be done with host time measurements. You can use real time elapsed or CPU time elapsed ([see this C++ documentation for reference](https://en.cppreference.com/w/cpp/chrono/c/clock))

```cpp
  std::clock_t c_start = std::clock();
  ... //what needs to be measured
  std::clock_t c_end = std::clock();
  1000.0 * (c_end - c_start) / CLOCKS_PER_SEC //in ms
```

is the CPU wallclock time and

```cpp
  auto t_start = std::chono::high_resolution_clock::now();
  ... //what needs to be measured
  auto t_end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double, std::milli>(t_end - t_start).count() //in ms
```

is the real time version.

### How to measure literally anything that happens on the device

The program nvprof is the nvidia profiler and uses inbuilt sensors/processors to measure anything imaginable.
For a full list of available metrics invoke 

```bash
nvprof --query-metrics
```

If you search for a specific metric pipe the output through ``grep``, e.g.

```bash
nvprof --query-metrics | grep flop
```

will display every metric that contain in its name or description the word flop.

The following example shows how to use nvprof with the program of this repo to
measure flops and the bytes that are read and written by the program.

```bash
nvprof --metrics flop_sp_efficiency,flop_count_sp,dram_read_bytes,dram_write_bytes -f ./run_test pinned comp 1 float measurement_10_comp-kernel_template
```

If the nvidia visual profiler (nvvp) is working, you can also output a binary that can be read by nvvp.
There you can work with a nice GUI to keep track of the desired metrics. To export the binary pass the ``-o`` flag
followed by the filename. 

For more information about ``nvprof`` check ``nvprof --help``.


## Nvidia V100 Specs & Benchmarking


| Spec                    | Limit |
|-------------------------|-------|
| Peak TFLOP/s double     |   7   |
| Peak TFLOP/s float      |   14  |
| Peak TFLOP/s half  	  |   28  |
| Bandwidth Device        |  900  |
| Bandwidth Host<->Device | 15.75 |

The specs above are the theoretical achievable hardware limits (half precision is not stated explicitly).
Further details can be seen [in this post (Tesla V100 PCIe).](https://wccftech.com/nvidia-tesla-v100s-server-gpu-16-tflops-over-1tb-hbm2/)
The following tests show, that you can approximately obtain them by suitable kernels with the measurements technique defined above.
To obtain the theoretical peak performance, a sufficiently high arithmetic intensity must be given, such that the program is not bounded by the bandwidth
Arithmetic Intensity describes the FLOP/s per byte and so, different arithmetic intensities for different datatypes are necessary. 

| Datatype | Peak Performance | Arithmetic Intensity(Peak FLOP/s by Datamovement) |
|----------|------------------|---------------------------------------------------|
| double   | 7 TFLOP/s		  | 7.77 (FLOP/(s$\cdot$byte)						  |
| float     | 14 TFLOP/s       | 15.55 (FLOP/(s$\cdot$byte) 				       | 
| half 	   | 28 TFLOP/s 	  | 25.55 (FLOP/(s$\cdot$byte)						  |


The above table assumes the HBM2 (High Bandwidth Memory 2) ceiling, i.e. 900 GB/s. 
First, the bandwidth between the device and its memory is tested. We'd like to see a peak near 900 GB/s.
For this, the add kernel is utilized, since it is by design bandwidth limited. 
The bytes read and written by the kernel are measured by ``nvprof`` and divided by the measured time (``cudaEvents``)


![](notebooks/figures/add_kernel_bandwidth.png){width=90%}


The figure above shows the effective bandwidth, i.e. the bandwidth metric that is smeared out by kernel invocations, driver initializations and 1 FLOP per element trough the addition. However, the obtained result is close to the [hardware limits according to Nvidia](https://www.nvidia.com/en-us/data-center/v100/)



To measure the peak FLOP/s the ``comp`` kernel FLOP are measured and divided by the ``cudaEvent`` time measurement of the kernel. Again, this is a smeared out version due to kernel invocations, driver initializations and several read and write operations.


![](notebooks/figures/comp_kernel_flops.png){width=90%}


The figure above shows the result. The Nvidia profiler ``nvprof`` shows for each case an efficiency of 60% and thus it can be concluded that there are still to many read and write operations.
However, the chosen kernel is already fairly academic.
So, further arithmetic operations that **don't vanish by compiler optimization and don't require further read and write operations** need to be included in order to see the full peak FLOP/s.
The results are nonetheless in a reasonable magnitude.


Now, the largest vector sizes are taken for the ``comp`` kernel and drawn into the roofline model for the V100.

![](notebooks/figures/roofline.png){width=90%}

Further information how to measure and draw roofline models can be found [in this slides](https://cug.org/proceedings/cug2019_proceedings/includes/files/pap103s2-file2.pdf)


## Plot Measurements
In the directory ``notebooks`` you will find a jupyter-notebook that shows you how to plot properly the measured data. 
 
